import pyperclip
import os

def counter_clipboard_text():
    try:
        clipboard_content = pyperclip.paste()
        if os.path.isfile(clipboard_content):
            analyze_file(clipboard_content)
        else:
            analyze_text(clipboard_content)
    except Exception as e:
        print(f"This is neither text nor a file. Error: {e}")

def analyze_text(text: str):
    line_count = text.count('\n') + (not text.endswith('\n')) if text else 0
    word_count = len(text.split())
    char_count_with_spaces = len(text)
    char_count_without_spaces = len(text.replace(" ", ""))

    print("IN YOUR TEXT:\n")
    print(f"lines: {line_count}")
    print(f"words: {word_count}")
    print(f"characters with spaces: {char_count_with_spaces}")
    print(f"characters without spaces: {char_count_without_spaces}")

def analyze_file(file_path: str):
    if not file_path.endswith('.txt'):
        print(f"Unsupported file format: {os.path.splitext(file_path)[1]}")
        return

    file_name_props = get_filename_properties(file_path)

    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            content = file.read()
            if content.strip() == "":
                print(f"IN YOUR FILE ({os.path.splitext(file_path)[1]}) NO TEXT:\n")
                print_filename_properties(file_name_props)
            else:
                line_count = content.count('\n') + (not content.endswith('\n'))
                word_count = len(content.split())
                char_count_with_spaces = len(content)
                char_count_without_spaces = len(content.replace(" ", ""))

                print(f"IN YOUR FILE ({os.path.splitext(file_path)[1]}):\n")
                print(f"lines: {line_count}")
                print(f"words: {word_count}")
                print(f"characters with spaces: {char_count_with_spaces}")
                print(f"characters without spaces: {char_count_without_spaces}")
                print_filename_properties(file_name_props)

    except UnicodeDecodeError:
        print(f"YOUR FILE ({os.path.splitext(file_path)[1]}) CONTAINS TECHNICAL INFORMATION:\n")
        print_filename_properties(file_name_props)

def get_filename_properties(file_path: str):
    file_name_with_extension = os.path.basename(file_path)
    file_name_without_extension = os.path.splitext(file_name_with_extension)[0]
    return {
        'char_count_with_extension': len(file_name_with_extension),
        'char_count_without_extension': len(file_name_without_extension.replace(" ", "")),
        'spaces_count': file_name_without_extension.count(" ")
    }

def print_filename_properties(props: dict):
    print(f"characters in the filename with extension: {props['char_count_with_extension']}")
    print(f"characters in the filename without extension: {props['char_count_without_extension']}")
    print(f"spaces in the filename: {props['spaces_count']}")

if __name__ == "__main__":
    counter_clipboard_text()